module.exports = {
    client: {
        service: {
            name: "my-app",
            // URL to the GraphQL API
            url: "https://api.r-impact.com/query",
        },
        // Files processed by the extension
        includes: ["src/**/*.vue", "src/**/*.js", "src/**/*.graphql"],
    },
};