module.exports = {
    // variants: {
    //     borderWidth: ["responsive", "last", "hover", "focus", "group-hover"],
    // },
    variants: {
        textColor: ['responsive', 'hover', 'focus', 'group-hover'],
        borderWidth: ["responsive", "last", "hover", "focus"],
        backgroundColor: ['responsive', "odd", "even", 'hover', 'focus'],
        backgroundOpacity: ['responsive', "odd", "even", 'hover', 'focus'],
        padding: ['responsive'],
        textAlign: ['responsive', 'first', 'last'],
    },
    theme: {
        screens: {
            sm: "640px",// was 640px
            md: "1000px",//was 768px
            lg: "1024px",
            xl: "1280px",
        },
        fontFamily: {
            display: ["Gilroy", "sans-serif"],
            body: ["Graphik", "sans-serif"],
        },
        borderWidth: {
            default: "1px",
            "0": "0",
            "2": "2px",
            "4": "4px",
        },
        extend: {
            colors: {
                cyan: "#9cdbff",
                easiorange: "#E4572E",
                easipurple: "#183059",
                easidimOrange: "#ED947A",
                easidivider: " #576886"
            },
            spacing: {
                "90": "20rem",
                "96": "24rem",
                "98": "28rem",
                "100": "30rem",
                "128": "32rem",
            },
        },
    },
};