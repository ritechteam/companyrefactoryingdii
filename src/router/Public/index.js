const publicRoutes = [
    {
        path: "",
        name: "Login",
        component: () =>
            import ("../../views/main/Login.vue"),
    },
    {
        path: "register",
        name: "Register",
        component: () =>
            import ("../../views/main/Register.vue"),
    },
    {
        path: "success",
        name: "Success",
        component: () =>
            import ("../../views/main/Success.vue"),
    },
    {
        path: "reset-password",
        name: "Reset",
        component: () =>
            import ("../../views/main/ResetPassword.vue"),
    },
    {
        path: "forgot-password",
        name: "Forgot",
        component: () =>
            import ("../../views/main/ForgotPassword.vue"),
    },
];

export default publicRoutes;