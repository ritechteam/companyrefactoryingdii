const otherRoutes = [
    {
        path: "",
        name: "FirstOffer",
        component: () =>
            import ("../../views/main/FirstOffer.vue"),
    },
];

export default otherRoutes;