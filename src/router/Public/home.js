const homeRoutes = [
    {
        path: "",
        name: "Home",
        component: () =>
            import ("../../views/main/Home.vue"),
    },
];

export default homeRoutes;