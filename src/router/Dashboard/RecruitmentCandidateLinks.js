const RecruitmentCandidateLinks = [{
    path: 'to-review',
    name: "CandidateToReview",
    component: () =>
        import ('../../views/DashboardLinks/Recruitment/Candidates/ToReview'),
    meta: {
        requiresHeader: "Candidates"
    }
}, {
    path: 'in-process',
    name: "CandidateInProcess",
    component: () =>
        import ('../../views/DashboardLinks/Recruitment/Candidates/InProcess'),
    meta: {
        requiresHeader: "Candidates"
    }

}, {
    path: 'hired',
    name: "CandidateHired",
    component: () =>
        import ('../../views/DashboardLinks/Recruitment/Candidates/Hired'),
    meta: {
        requiresHeader: "Candidates"
    }

}, {
    path: 'rejected',
    name: "CandidateRejected",
    component: () =>
        import ('../../views/DashboardLinks/Recruitment/Candidates/Rejected'),
    meta: {
        requiresHeader: "Candidates"
    }

}, {
    path: 'view-candidates',
    component: () =>
        import ('../../views/DashboardLinks/Recruitment/Candidates/ViewCandidates'),
    redirect: {
        name: 'CandidateOverview'
    },

    children: [{
        path: 'Overview',
        name: 'CandidateOverview',
        component: () =>
            import ('../../views/DashboardLinks/Recruitment/Candidates/Overview'),
        meta: {
            requiresHeader: null
        },

    }, {
        path: 'profile',
        name: 'CandidateProfile',
        component: () =>
            import ('../../views/DashboardLinks/Recruitment/Candidates/Profile'),
        meta: {
            requiresHeader: null
        },
    }, {
        path: 'application',
        name: 'CandidateApplication',
        component: () =>
            import ('../../views/DashboardLinks/Recruitment/Candidates/Application'),
        meta: {
            requiresHeader: null
        },
    }, {
        path: 'email',
        name: 'CandidateEmail',
        component: () =>
            import ('../../views/DashboardLinks/Recruitment/Candidates/Email'),
        meta: {
            requiresHeader: null
        },
    }]
}]


export default RecruitmentCandidateLinks