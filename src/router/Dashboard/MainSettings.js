const RecruitmentSettings = [{
        path: "company",
        name: "SettingsCompany",
        component: () =>
            import ("../../views/DashboardLinks/Settings/Company"),
        meta: {
            requiresHeader: "SettingsHeader",
        },
    },
    {
        path: "profile",
        name: "SettingsProfile",
        component: () =>
            import ("../../views/DashboardLinks/Settings/Profile"),
        meta: {
            requiresHeader: "SettingsHeader",
        },
    },
    {
        path: "password",
        name: "SettingsPassword",
        component: () =>
            import ("../../views/DashboardLinks/Settings/Password"),
        meta: {
            requiresHeader: "SettingsHeader",
        },
    },
    {
        path: "bank-details",
        name: "SettingsBankDetails",
        component: () =>
            import ("../../views/DashboardLinks/Settings/BankDetailsView"),
        meta: {
            requiresHeader: "SettingsHeader",
        },
    },
    {
        path: "bank-details-edit",
        name: "SettingsBankDetailsEdit",
        component: () =>
            import ("../../views/DashboardLinks/Settings/BankDetailsEdit"),
        meta: {
            requiresHeader: "SettingsHeader",
        },
    },
    {
        path: "compliance",
        name: "SettingsCompliance",
        component: () =>
            import ("../../views/DashboardLinks/Settings/Compliance"),
        meta: {
            requiresHeader: "SettingsHeader",
        },
    },
    {
        path: "employee-privileges",
        name: "SettingsEmployeePrivileges",
        component: () =>
            import ("../../views/DashboardLinks/Settings/EmployeePrivileges"),
        meta: {
            requiresHeader: "SettingsHeader",
        },
    },
    {
        path: "assign-privileges",
        name: "SettingsAssignPrivileges",
        component: () =>
            import ("../../views/DashboardLinks/Settings/AssignPrivileges"),
        meta: {
            requiresHeader: "SettingsHeader",
        },
    },
    // {
    //     path: "notification",
    //     name: "SettingsNotifications",
    //     component: () =>
    //         import ("../../views/DashboardLinks/Settings/Notification"),
    //     meta: {
    //         requiresHeader: "SettingsHeader",
    //     },

    // },
    // {
    //     path: "automated-message",
    //     name: 'SettingsAutomated',
    //     component: () =>
    //         import ("../../views/DashboardLinks/Settings/AutomatedMessage"),
    //     meta: {
    //         requiresHeader: "SettingsHeader",
    //     },

    // },

];

export default RecruitmentSettings;