const Payroll = [
	{
		path: 'payments',
		name: 'PayrollPayments',
		component: () => import('../../../views/DashboardLinks/Payroll/Payments'),
		meta: {
			requiresHeader: 'Jobs',
		},
	},
	{
		path: 'benefits',
		name: 'PayrollBenefits',
		component: () => import('../../../views/DashboardLinks/Payroll/Benefits'),
		meta: {
			requiresHeader: 'Jobs',
		},
	},
	{
		path: 'staff-level',
		name: 'PayrollStaffLevel',
		component: () => import('../../../views/DashboardLinks/Payroll/StaffLevel'),
		meta: {
			requiresHeader: 'Jobs',
		},
	},
	{
		path: 'deductions',
		name: 'PayrollDeductions',
		component: () => import('../../../views/DashboardLinks/Payroll/Deductions'),
		meta: {
			requiresHeader: 'Jobs',
		},
	},
	{
		path: 'create-schedule',
		name: 'PayrollCreateSchedule',
		component: () =>
			import('../../../views/DashboardLinks/Payroll/CreateSchedule'),
	},
	{
		path: 'add-payroll',
		name: 'PayrollAddPayroll',
		component: () => import('../../../views/DashboardLinks/Payroll/AddPayroll'),
	},
	{
		path: 'add-payroll-two',
		name: 'PayrollAddPayrollTwo',
		component: () =>
			import('../../../views/DashboardLinks/Payroll/AddPayrollTwo'),
	},
	{
		path: 'create-benefit',
		name: 'PayrollCreateBenefit',
		component: () =>
			import('../../../views/DashboardLinks/Payroll/CreateBenefit'),
	},
	{
		path: 'edit-benefit/:name/:id',
		name: 'PayrollEditBenefit',
		component: () =>
			import('../../../views/DashboardLinks/Payroll/EditBenefit'),
	},
	{
		path: 'create-staff-level',
		name: 'PayrollCreateStaffLevel',
		component: () =>
			import('../../../views/DashboardLinks/Payroll/CreateStaffLevel'),
	},
	{
		path: 'assign-to-staff',
		name: 'PayrollAssignToStaff',
		component: () =>
			import('../../../views/DashboardLinks/Payroll/AssignToStaff'),
	},
	{
		path: 'create-deduction',
		name: 'PayrollCreateDeduction',
		component: () =>
			import('../../../views/DashboardLinks/Payroll/CreateDeduction'),
	},
];

export default Payroll;
import PerformanceOverview from './PerformanceOverview';
import PerformanceList from './PerformanceList';
import PerformanceEmployees from './PerformanceEmployees.js';

const Performance = [
	{
		path: 'Performance-Overview',
		component: () =>
			import('../../../views/DashboardLinks/Performance/Overview/Overview'),
		redirect: {
			name: 'Overview',
		},
		children: PerformanceOverview,
	},
	{
		path: 'Performance-List',
		component: () =>
			import(
				'../../../views/DashboardLinks/Performance/PerformanceList/PerformanceList'
			),
		redirect: {
			name: 'PerformanceList',
		},
		children: PerformanceList,
	},
	{
		path: 'Performance-Employees',
		component: () =>
			import('../../../views/DashboardLinks/Performance/Employees/Employees'),
		redirect: {
			name: 'Employees',
		},
		children: PerformanceEmployees,
	},
];

export default Performance;
