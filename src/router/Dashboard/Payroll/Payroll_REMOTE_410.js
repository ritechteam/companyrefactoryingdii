import PerformanceOverview from './PerformanceOverview'
import PerformanceList from './PerformanceList'
import PerformanceEmployees from './PerformanceEmployees.js'

const Performance = [{
        path: 'Performance-Overview',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/Overview/Overview"),
        redirect: {
            name: 'Overview'
        },
        children: PerformanceOverview
    },
    {
        path: 'Performance-List',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/PerformanceList/PerformanceList"),
        redirect: {
            name: 'PerformanceList'
        },
        children: PerformanceList
    },
    {
        path: 'Performance-Employees',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/Employees/Employees"),
        redirect: {
            name: 'Employees'
        },
        children: PerformanceEmployees
    }
]


export default Performance