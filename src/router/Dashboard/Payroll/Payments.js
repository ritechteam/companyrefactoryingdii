const Payments = [
    {
        path: "",
        name: "PayrollPaymentsAll",
        component: () =>
            import("../../../views/DashboardLinks/Payroll/Payments/All"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "paye",
        name: "PayrollPaymentsPAYE",
        component: () =>
            import("../../../views/DashboardLinks/Payroll/Payments/PAYE"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "pension",
        name: "PayrollPaymentsPension",
        component: () =>
            import("../../../views/DashboardLinks/Payroll/Payments/Pension"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "nhf",
        name: "PayrollPaymentsNHF",
        component: () =>
            import("../../../views/DashboardLinks/Payroll/Payments/NHF"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "employee-life-insurance",
        name: "PayrollPaymentsEmployeeLifeInsurance",
        component: () =>
            import("../../../views/DashboardLinks/Payroll/Payments/EmployeeLifeInsurance"),
        meta: {
            requiresHeader: "Jobs",
        },
    },

];

export default Payments;