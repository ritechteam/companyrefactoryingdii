import Payments from './Payments'

const Payroll = [
	{
		path: '',
		name: 'PayrollOverview',
		component: () => import('../../../views/DashboardLinks/Payroll/Overview/Overview'),
	},
	
	// Payroll Settings Routes:
	{
		path: 'settings',
		name: 'PayrollSettings',
		component: () => import('../../../views/DashboardLinks/Payroll/Settings/StatutoryBodies'),
	},
	{
		path: 'create-provision',
		name: 'PayrollSettingsCreateProvision',
		component: () => import('../../../views/DashboardLinks/Payroll/Settings/CreateProvision'),
	},
	{
		path: 'list-statutory-bodies/:provisionId',
		name: 'PayrollSettingsListStatutoryBodies',
		component: () => import('../../../views/DashboardLinks/Payroll/Settings/ListStatutoryBodies'),
	},
	{
		path: 'edit-statutory-body/:statutorybodyId',
		name: 'PayrollSettingsEditStatutoryBody',
		component: () => import('../../../views/DashboardLinks/Payroll/Settings/EditStatutoryBody'),
	},
	{
		path: 'add-statutory-body/:id',
		name: 'PayrollSettingsAddStatutoryBody',
		component: () => import('../../../views/DashboardLinks/Payroll/Settings/EditStatutoryBody'),
	},
	// {
	// 	path: 'settings',
	// 	// name: '', 
	// 	component: () => import('../../../views/DashboardLinks/Payroll/Settings/StatutoryBodies'),
	// 	redirect: {
    //         name: 'PayrollSettings'
    //     },
	// 	children: PayrollSettings
	// },
	
	
	{
		path: 'allowances',
		name: 'PayrollAllowances',
		component: () => import('../../../views/DashboardLinks/Payroll/Allowances/Allowances'),
	},
	{
		path: 'create-allowance',
		name: 'CreateAllowance',
		component: () => import('../../../views/DashboardLinks/Payroll/Allowances/CreateAllowance'),
	},

	// Benefits Routes:
	{
		path: 'benefits',
		name: 'PayrollBenefits',
		component: () => import('../../../views/DashboardLinks/Payroll/Benefits/Benefits'),
	},
	{
		path: 'create-benefits',
		name: 'CreateBenefits',
		component: () => import('../../../views/DashboardLinks/Payroll/Benefits/CreateBenefits'),
	},

	// OtherPayments Routes: Removed for now.
	// {
	// 	path: 'other-payments',
	// 	name: 'PayrollOtherPayments',
	// 	component: () => import('../../../views/DashboardLinks/Payroll/OtherPayments/OtherPayments'),
	// },
	// {
	// 	path: 'create-other-payments',
	// 	name: 'PayrollCreateOtherPayments',
	// 	component: () => import('../../../views/DashboardLinks/Payroll/OtherPayments/CreateOtherPayments'),
	// },

	// Stafflevel Routes:
	{
		path: 'stafflevel',
		name: 'PayrollStaffLevel',
		component: () => import('../../../views/DashboardLinks/Payroll/StaffLevel/StaffLevel'),
	},
	{
		path: 'create-stafflevel',
		name: 'PayrollCreateStaffLevel',
		component: () => import('../../../views/DashboardLinks/Payroll/StaffLevel/CreateStaffLevel'),
	},
	{
		path: 'view-stafflevel/:id',
		name: 'PayrollViewStaffLevel',
		component: () => import('../../../views/DashboardLinks/Payroll/StaffLevel/ViewStaffLevel'),
	},
	{
		path: 'update-stafflevel/:id',
		name: 'PayrollUpdateStaffLevel',
		component: () => import('../../../views/DashboardLinks/Payroll/StaffLevel/UpdateStaffLevel'),
	},

	// Payments Routes:
	{
		path: 'payments',
		component: () =>
			import ('../../../views/DashboardLinks/Payroll/Payments/Payments'),
		redirect: {
			name: 'PayrollPaymentsAll'
		},
		children: Payments
	},
	{
		path: 'employees',
		name: 'PayrollPaymentsEmployees',
		component: () => import('../../../views/DashboardLinks/Payroll/Payments/ViewPayrollEmployees'),
	},
	{
		path: 'payments-edit',
		name: 'PayrollPaymentsEdit',
		component: () => import('../../../views/DashboardLinks/Payroll/Payments/EditPayments'),
	},
	{
		path: 'view-paye',
		name: 'PayrollPaymentsViewPAYE',
		component: () => import('../../../views/DashboardLinks/Payroll/Payments/ViewPAYEPayments'),
	},
	{
		path: 'view-pension',
		name: 'PayrollPaymentsViewPension',
		component: () => import('../../../views/DashboardLinks/Payroll/Payments/ViewPensionPayments'),
	},
	{
		path: 'view-nhf',
		name: 'PayrollPaymentsViewNHF',
		component: () => import('../../../views/DashboardLinks/Payroll/Payments/ViewNHFPayments'),
	},
	{
		path: 'view-employee-life-insurance',
		name: 'PayrollPaymentsViewEmployeeLifeInsurance',
		component: () => import('../../../views/DashboardLinks/Payroll/Payments/ViewEmployeeLifeInsurancePayments'),
	},

	// PaySlip Route:
	{
		path: 'payslip',
		name: 'PayrollPaySlipList',
		component: () => import('../../../views/DashboardLinks/Payroll/PaySlip/PaySlipList'),
	},
	{
		path: 'payslip-view',
		name: 'PayrollPaySlipView',
		component: () => import('../../../views/DashboardLinks/Payroll/PaySlip/PaySlipView'),
	},


	// *Note: Old Routes below.
	// {
	// 	path: 'payments',
	// 	name: 'PayrollPayments',
	// 	component: () => import('../../../views/DashboardLinks/Payroll/Payments'),
	// 	meta: {
	// 		requiresHeader: 'Jobs',
	// 	},
	// },
	// {
	// 	path: 'benefits',
	// 	name: 'PayrollBenefits',
	// 	component: () => import('../../../views/DashboardLinks/Payroll/Benefits'),
	// 	meta: {
	// 		requiresHeader: 'Jobs',
	// 	},
	// },
	// {
	// 	path: 'staff-level',
	// 	name: 'PayrollStaffLevel',
	// 	component: () => import('../../../views/DashboardLinks/Payroll/StaffLevel'),
	// 	meta: {
	// 		requiresHeader: 'Jobs',
	// 	},
	// },
	// {
	// 	path: 'deductions',
	// 	name: 'PayrollDeductions',
	// 	component: () => import('../../../views/DashboardLinks/Payroll/Deductions'),
	// 	meta: {
	// 		requiresHeader: 'Jobs',
	// 	},
	// },
	// {
	// 	path: 'create-schedule',
	// 	name: 'PayrollCreateSchedule',
	// 	component: () =>
	// 		import('../../../views/DashboardLinks/Payroll/CreateSchedule'),
	// },
	// {
	// 	path: 'add-payroll',
	// 	name: 'PayrollAddPayroll',
	// 	component: () => import('../../../views/DashboardLinks/Payroll/AddPayroll'),
	// },
	// {
	// 	path: 'add-payroll-two',
	// 	name: 'PayrollAddPayrollTwo',
	// 	component: () =>
	// 		import('../../../views/DashboardLinks/Payroll/AddPayrollTwo'),
	// },
	// {
	// 	path: 'create-benefit',
	// 	name: 'PayrollCreateBenefit',
	// 	component: () =>
	// 		import('../../../views/DashboardLinks/Payroll/CreateBenefit'),
	// },
	// {
	// 	path: 'edit-benefit/:name/:id',
	// 	name: 'PayrollEditBenefit',
	// 	component: () =>
	// 		import('../../../views/DashboardLinks/Payroll/EditBenefit'),
	// },
	// {
	// 	path: 'create-staff-level',
	// 	name: 'PayrollCreateStaffLevel',
	// 	component: () =>
	// 		import('../../../views/DashboardLinks/Payroll/CreateStaffLevel'),
	// },
	// {
	// 	path: 'assign-to-staff',
	// 	name: 'PayrollAssignToStaff',
	// 	component: () =>
	// 		import('../../../views/DashboardLinks/Payroll/AssignToStaff'),
	// },
	// {
	// 	path: 'create-deduction',
	// 	name: 'PayrollCreateDeduction',
	// 	component: () =>
	// 		import('../../../views/DashboardLinks/Payroll/CreateDeduction'),
	// },
];

export default Payroll;
