const Feedbacks = [
    {
        path: "all",
        name: "FeedbacksAll",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Feedbacks/All"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "received",
        name: "FeedbacksReceived",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Feedbacks/Received"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "sent",
        name: "FeedbacksSent",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Feedbacks/Sent"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "new-message",
        name: "FeedbacksNewMessage",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Feedbacks/NewMessage"),

    },
    {
        path: "view-message",
        name: "FeedbacksViewMessage",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Feedbacks/ViewMessage"),

    },

];

export default Feedbacks;