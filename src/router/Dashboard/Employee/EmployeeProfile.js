const EmployeeProfile = [
    {
        path: "profile/:id",
        name: "ViewEmployeeProfile",
        component: () =>
            import("../../../views/DashboardLinks/Employee/EmployeeProfile/Profile"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "job/:id",
        name: "ViewEmployeeJob",
        component: () =>
            import("../../../views/DashboardLinks/Employee/EmployeeProfile/Job"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "bank/:id",
        name: "ViewEmployeeBank",
        component: () =>
            import("../../../views/DashboardLinks/Employee/EmployeeProfile/Bank"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "statutory-body/:id",
        name: "ViewEmployeeStatutoryBody",
        component: () =>
            import("../../../views/DashboardLinks/Employee/EmployeeProfile/StatutoryBody"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "emergency/:id",
        name: "ViewEmployeeEmergency",
        component: () =>
            import("../../../views/DashboardLinks/Employee/EmployeeProfile/Emergency"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "referees/:id",
        name: "ViewEmployeeReferees",
        component: () =>
            import("../../../views/DashboardLinks/Employee/EmployeeProfile/Referees"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
];

export default EmployeeProfile;