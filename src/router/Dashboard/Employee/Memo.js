const Memo = [
	{
		path: "all",
		name: "MemoAll",
		component: () => import("../../../views/DashboardLinks/Employee/Memo/All"),
		meta: {
			requiresHeader: "Jobs",
		},
	},
	{
		path: "approvals",
		name: "MemoApprovals",
		component: () =>
			import("../../../views/DashboardLinks/Employee/Memo/Approvals"),
		meta: {
			requiresHeader: "Jobs",
		},
	},
	{
		path: "sent",
		name: "MemoSent",
		component: () => import("../../../views/DashboardLinks/Employee/Memo/Sent"),
		meta: {
			requiresHeader: "Jobs",
		},
	},
	{
		path: "received",
		name: "MemoReceived",
		component: () =>
			import("../../../views/DashboardLinks/Employee/Memo/Received"),
		meta: {
			requiresHeader: "Jobs",
		},
	},
	{
		path: "templates",
		name: "MemoTemplates",
		component: () =>
			import("../../../views/DashboardLinks/Employee/Memo/Templates"),
		meta: {
			requiresHeader: "Jobs",
		},
	},
	{
		path: "create-memo",
		name: "MemoCreateMemo",
		component: () =>
			import("../../../views/DashboardLinks/Employee/Memo/CreateMemo"),
	},
	{
		path: "edit-memo",
		name: "MemoEditMemo",
		component: () =>
			import("../../../views/DashboardLinks/Employee/Memo/EditMemo"),
	},
	{
		path: "view-memo",
		name: "MemoViewMemo",
		component: () =>
			import("../../../views/DashboardLinks/Employee/Memo/ViewMemo"),
	},
	{
		path: "create-template",
		name: "MemoCreateTemplate",
		component: () =>
			import("../../../views/DashboardLinks/Employee/Memo/CreateTemplate"),
	},
	{
		path: "edit-template/:id",
		name: "MemoEditTemplate",
		component: () =>
			import("../../../views/DashboardLinks/Employee/Memo/EditTemplate"),
	},
];

export default Memo;
