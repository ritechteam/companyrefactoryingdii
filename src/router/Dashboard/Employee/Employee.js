import Memo from './Memo'
import Tasks from './Tasks'
import Feedback from './Feedback'
import Applications from './Applications'

import Overview from './Overview'
import OnboardEmployee from './OnboardEmployee'
import EmployeeProfile from './EmployeeProfile'

// import Attendance from './Attendance'
// import Feedback from './Feedback'



const Employee = [
{
    path: 'memo',
    component: () =>
        import('../../../views/DashboardLinks/Employee/Memo/Memo'),
    redirect: {
        name: 'MemoAll'
    },
    children: Memo
},

{
    path: 'tasks',
    component: () =>
        import('../../../views/DashboardLinks/Employee/Tasks/Tasks'),
    redirect: {
        name: 'TasksActive'
    },
    children: Tasks
},
{
    path: 'feedbacks',
    component: () =>
        import('../../../views/DashboardLinks/Employee/Feedbacks/Feedbacks'),
    redirect: {
        name: 'FeedbacksAll'
    },
    children: Feedback
},
{
    path: 'applications',
    component: () =>
        import ('../../../views/DashboardLinks/Employee/Applications/Applications'),
    redirect: {
        name: 'EmployeeApplicationsAll'
    },
    children: Applications
},
{
    path: 'overview',
    component: () =>
        import ('../../../views/DashboardLinks/Employee/Overview/Overview'),
    redirect: {
        name: 'EmployeeOverviewList'
    },
    children: Overview
},
{
    path: 'onboard',
    component: () =>
        import ('../../../views/DashboardLinks/Employee/OnboardEmployee/CreateEmployee'),
    redirect: {
        name: 'CreateEmployeeProfile'
    },
    children: OnboardEmployee
},
{
    path: 'view',
    component: () =>
        import ('../../../views/DashboardLinks/Employee/EmployeeProfile/ViewEmployee'),
    redirect: {
        name: 'ViewEmployeeProfile'
    },
    children: EmployeeProfile
},



    // }, {
    //     path: 'attendance',
    //     component: () =>
    //         import ('../../../views/DashboardLinks/Employee/Attendance/Attendance'),
    //     redirect: {
    //         name: 'RecruitmentAssessmentsMain'
    //     },
    //     children: Attendance
    // }, {
    //     path: 'payroll',
    //     component: () =>
    //         import ('../../../views/DashboardLinks/Employee/Payroll/Payroll'),
    //     redirect: {
    //         name: 'SettingsCompany'
    //     },
    //     children: Payroll
    // },
    // {
    //     path: 'tasks',
    //     component: () =>
    //         import ('../../../views/DashboardLinks/Employee/Tasks/Tasks'),
    //     redirect: {
    //         name: 'SettingsCompany'
    //     },
    //     children: Tasks
    // },
 
    // {
    //     path: 'feedback',
    //     component: () =>
    //         import ('../../../views/DashboardLinks/Employee/Feedbacks/Feedbacks'),
    //     redirect: {
    //         name: 'SettingsCompany'
    //     },
    //     children: Feedback
    // }

]

export default Employee