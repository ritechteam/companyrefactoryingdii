const Applications = [
    {
        path: "all",
        name: "EmployeeApplicationsAll",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Applications/All"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "received",
        name: "EmployeeApplicationsReceived",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Applications/Received"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "sent",
        name: "EmployeeApplicationsSent",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Applications/Sent"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "new",
        name: "EmployeeApplicationsNew",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Applications/New"),

    },
    {
        path: "view",
        name: "EmployeeApplicationsView",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Applications/View"),

    },
    {
        path: "reply",
        name: "EmployeeApplicationsReply",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Applications/Reply"),

    },

];

export default Applications;