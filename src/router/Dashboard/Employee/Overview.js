const Overview = [
    {
        path: "list",
        name: "EmployeeOverviewList",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Overview/List"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "branch",
        name: "EmployeeOverviewBranch",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Overview/Branch"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "department",
        name: "EmployeeOverviewDepartment",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Overview/Department"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "team",
        name: "EmployeeOverviewTeam",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Overview/Team"),
        meta: {
            requiresHeader: "Jobs",
        },

    },
    {
        path: "create-branch",
        name: "CreateBranch",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Overview/CreateBranch"),

    },
    {
        path: "create-department",
        name: "CreateDepartment",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Overview/CreateDepartment"),

    },
    {
        path: "create-team",
        name: "CreateTeam",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Overview/CreateTeam"),

    },

];

export default Overview;