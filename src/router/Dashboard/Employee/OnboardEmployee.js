const OnboardEmployee = [
    {
        path: "profile",
        name: "CreateEmployeeProfile",
        component: () =>
            import("../../../views/DashboardLinks/Employee/OnboardEmployee/EmployeeProfile"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "job",
        name: "CreateEmployeeJob",
        component: () =>
            import("../../../views/DashboardLinks/Employee/OnboardEmployee/Job"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "bank",
        name: "CreateEmployeeBank",
        component: () =>
            import("../../../views/DashboardLinks/Employee/OnboardEmployee/Bank"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "emergency",
        name: "CreateEmployeeEmergency",
        component: () =>
            import("../../../views/DashboardLinks/Employee/OnboardEmployee/Emergency"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
];

export default OnboardEmployee;