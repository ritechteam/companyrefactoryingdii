const Tasks = [
    {
        path: "active",
        name: "TasksActive",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Tasks/Active"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "received",
        name: "TasksReceived",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Tasks/Received"),
        meta: {
            requiresHeader: "Jobs",
        },
    },
    {
        path: "completed",
        name: "TaskCompleted",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Tasks/Completed"),
        meta: {
            requiresHeader: "Jobs",
        },
    },

    {
        path: "new-task",
        name: "TasksNewTask",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Tasks/NewTask"),

    },
    {
        path: "view-task",
        name: "TasksViewTask",
        component: () =>
            import("../../../views/DashboardLinks/Employee/Tasks/ViewTask"),

    },


];

export default Tasks;