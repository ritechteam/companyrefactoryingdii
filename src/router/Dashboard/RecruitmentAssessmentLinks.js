const RecruitmentAssessmentLinks = [{
        path: "assessments",
        name: "RecruitmentAssessmentsMain",
        component: () =>
            import ("../../views/DashboardLinks/Recruitment/RecruitmentAssessment/RecruitmentAssessments"),
        meta: {
            requiresHeader: "RecruitmentAssessmentHeader",
        },
    },
    {
        path: "categories",
        name: "RecruitmentCategories",
        component: () =>
            import ("../../views/DashboardLinks/Recruitment/RecruitmentAssessment/RecruitmentCategories"),
        meta: {
            requiresHeader: "RecruitmentAssessmentHeader",
        },
    },
    {
        path: "view-questions",
        name: "ViewQuestions",
        component: () =>
            import ("../../views/DashboardLinks/Recruitment/RecruitmentAssessment/ViewQuestions"),
    },
    {
        path: "create-assessment-questions",
        component: () =>
            import ("../../views/DashboardLinks/Recruitment/RecruitmentAssessment/CreateAssessmentQuestions"),
        redirect: {
            name: "MultipleChoice",
        },

        children: [{
                path: "multiple-choice",
                name: "MultipleChoice",
                component: () =>
                    import ("../../views/DashboardLinks/Recruitment/RecruitmentAssessment/MultipleChoice"),
                meta: {
                    requiresHeader: null,
                },
            },
            {
                path: "true-or-false",
                name: "TrueOrFalse",
                component: () =>
                    import ("../../views/DashboardLinks/Recruitment/RecruitmentAssessment/TrueOrFalse"),
                meta: {
                    requiresHeader: null,
                },
            },
            {
                path: "Essay",
                name: "Essay",
                component: () =>
                    import ("../../views/DashboardLinks/Recruitment/RecruitmentAssessment/Essay"),
                meta: {
                    requiresHeader: null,
                },
            },
        ],
    },
];

export default RecruitmentAssessmentLinks;