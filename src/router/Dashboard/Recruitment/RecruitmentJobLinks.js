const RecruitmentJobLinks = [
	{
		path: 'active',
		name: 'RecruitmentActive',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/Active'),
		meta: {
			requiresHeader: 'Jobs',
		},
	},
	{
		path: 'draft',
		name: 'RecruitmentDraft',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/Draft'),
		meta: {
			requiresHeader: 'Jobs',
		},
	},
	{
		path: 'archived',
		name: 'RecruitmentArchived',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/Archived'),
		meta: {
			requiresHeader: 'Jobs',
		},
	},
	{
		path: 'all-aplicants/:id',
		name: 'RecruitmentAllApplicants',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/AllApplicants'),
		meta: {
			requiresHeader: false,
		},
	},
	{
		path: 'preview',
		name: 'RecruitmentPostJobPreview',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/PostJobPreview'),
		meta: {
			requiresHeader: false,
		},
	},
	{
		path: 'edit-preview',
		name: 'EditRecruitmentPostJobPreview',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/EditJobPreview'),
		meta: {
			requiresHeader: false,
		},
	},
	{
		path: 'post-jobs',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/PostJob'),
		redirect: {
			name: 'RecruitmentGeneralJobInformation',
		},

		children: [
			{
				path: 'general-job-information',
				name: 'RecruitmentGeneralJobInformation',
				meta: {
					requiresHeader: null,
				},
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/GeneralJobInformation'),
			},
			{
				path: 'candidate-background',
				name: 'RecruitmentCandidateBackground',
				meta: {
					requiresHeader: null,
				},
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/CandidateBackground'),
			},
			{
				path: 'job-description',
				name: 'RecruitmentJobDescription',
				meta: {
					requiresHeader: null,
				},
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/JobDescription'),
			},
			{
				path: 'assessment',
				redirect: {
					name: 'RecruitmentAssessment',
				},
				meta: {
					requiresHeader: null,
				},
				component: () => import('../../../views/DashboardLinks/Recruitment/jobs/Assessment'),
				children: [
					{
						path: 'publish',
						name: 'RecruitmentAssessment',
						component: () =>
							import('../../../views/DashboardLinks/Recruitment/jobs/PublishJob'),
					},
					{
						path: 'add-assessment',
						name: 'RecruitmentAddAssessment',
						component: () =>
							import('../../../views/DashboardLinks/Recruitment/jobs/AddAssessment'),
					},
				],
			},
		],
	},
	{
		path: 'edit-jobs',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/EditJob'),
		redirect: {
			name: 'EditRecruitmentGeneralJobInformation',
		},

		children: [
			{
				path: 'general-job-information',
				name: 'EditRecruitmentGeneralJobInformation',
				meta: {
					requiresHeader: null,
				},
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/EditGeneralJobInformation'),
			},
			{
				path: 'candidate-background',
				name: 'EditRecruitmentCandidateBackground',
				meta: {
					requiresHeader: null,
				},
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/EditCandidateBackground'),
			},
			{
				path: 'job-description',
				name: 'EditRecruitmentJobDescription',
				meta: {
					requiresHeader: null,
				},
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/EditJobDescription'),
			},
			{
				path: 'assessment',
				redirect: {
					name: 'EditRecruitmentAssessment',
				},
				meta: {
					requiresHeader: null,
				},
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/EditAssessment'),
				children: [
					{
						path: 'publish',
						name: 'EditRecruitmentAssessment',
						component: () =>
							import('../../../views/DashboardLinks/Recruitment/jobs/EditPublishJob'),
					},
					{
						path: 'add-assessment',
						name: 'EditRecruitmentAddAssessment',
						component: () =>
							import('../../../views/DashboardLinks/Recruitment/jobs/EditAddAssessment'),
					},
				],
			},
		],
	},
	{
		path: 'request-applicant',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/RequestApplicant'),
		redirect: {
			name: 'RecruitmentRequestApplicantRequest',
		},
		children: [
			{
				path: 'request',
				name: 'RecruitmentRequestApplicantRequest',
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/RequestApplicantRequest'),
			},
			{
				path: 'assessment',
				name: 'RecruitmentRequestApplicantAssessment',
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/RequestApplicantAssessment'),
			},
		],
	},
	{
		path: 'view-applicant',
		component: () => import('../../../views/DashboardLinks/Recruitment/jobs/ViewApplicant'),
		redirect: {
			name: 'RecruitmentViewApplicantOverview',
		},
		children: [
			{
				path: 'overview',
				name: 'RecruitmentViewApplicantOverview',
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/ViewApplicantOverview'),
			},
			{
				path: 'profile',
				name: 'RecruitmentViewApplicantProfile',
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/ViewApplicantProfile'),
			},
			{
				path: 'application',
				name: 'RecruitmentViewApplicantApplication',
				component: () =>
					import('../../../views/DashboardLinks/Recruitment/jobs/ViewApplicantApplication'),
			},
		],
	},
];

export default RecruitmentJobLinks;
