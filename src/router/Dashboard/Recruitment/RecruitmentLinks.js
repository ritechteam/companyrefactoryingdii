import RecruitmentJobLinks from "./RecruitmentJobLinks";
import RecruitmentCandidateLinks from "../RecruitmentCandidateLinks";
import RecruitmentAssessmentLinks from "../RecruitmentAssessmentLinks";

const RecruitmentLinks = [
	{
		path: "jobs",
		component: () => import("../../../views/DashboardLinks/Recruitment/jobs/Jobs.vue"),
		redirect: {
			name: "RecruitmentActive",
		},
		children: RecruitmentJobLinks,
	},
	{
		path: "candidates",
		component: () =>
			import("../../../views/DashboardLinks/Recruitment/Candidates/Candidates.vue"),
		redirect: {
			name: "CandidateToReview",
		},
		children: RecruitmentCandidateLinks,
	},
	{
		path: "assessments",
		component: () =>
			import(
				"../../../views/DashboardLinks/Recruitment/RecruitmentAssessment/Assessments.vue"
			),
		redirect: {
			name: "RecruitmentAssessmentsMain",
		},
		children: RecruitmentAssessmentLinks,
	},
];

export default RecruitmentLinks;
