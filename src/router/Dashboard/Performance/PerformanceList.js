const PerformanceList = [{
    path: "list",
    name: "PerformanceList",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/List"),
    meta: {
        requiresHeader: "Jobs",
    },
},
{
    path: "objectives",
    name: "PerformanceObjectives",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/Objectives"),
    meta: {
        requiresHeader: "Jobs",
    },
},
{
    path: "myperformance",
    name: "MyPerformance",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/MyPerformance"),
    meta: {
        requiresHeader: "Jobs",
    },
},
{
    path: "performance-categories",
    name: "PerformanceCategories",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/PerformanceCategories"),
    meta: {
        requiresHeader: "Jobs",
    },
},
];

export default PerformanceList;