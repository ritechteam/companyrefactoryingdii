const CreateCategory = [{
    path: "",
    name: "CreateCategory",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/CreateCategory"),
    meta: {
        requiresHeader: "Jobs",
    },
}
];

export default CreateCategory