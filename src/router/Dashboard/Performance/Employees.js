const Employees = [{
    path: "",
    name: "Employees",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/Employees/Employees"),
    meta: {
        requiresHeader: "Jobs",
    },
}
];

export default Employees;