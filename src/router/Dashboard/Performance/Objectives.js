const Objectives = [{
    path: "list",
    name: "Objectives",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/Objective"),
    meta: {
        requiresHeader: "Jobs",
    },
}

];

export default Objectives;