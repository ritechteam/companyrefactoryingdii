const CreateObjective = [{
    path: "",
    name: "CreateObjective",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/CreateObjective"),
    meta: {
        requiresHeader: "Jobs",
    },
}
];

export default CreateObjective