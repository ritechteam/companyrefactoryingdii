import PerformanceOverview from './PerformanceOverview'
import PerformanceList from './PerformanceList'
import Employees from './Employees'

import ViewGoals from './ViewGoals'
import InputReport from './InputReport'
import CreateGoal from './CreateGoal'

import EmployeePerformance from './EmployeePerformance'

import ViewObjective from './ViewObjective'
import CreateObjective from './CreateObjective'

import CreateCategory from './CreateCategory'


const Performance = [
    {
        path: 'Performance-Overview',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/Overview/Overview"),
        redirect: {
            name: 'Overview'
        },
        children: PerformanceOverview
    },
    {
        path: 'Performance-List',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/PerformanceList/PerformanceList"),
        redirect: {
            name: 'PerformanceCategories'
        },
        children: PerformanceList
    },
    {
        path: 'Employees',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/Employees/Employees"),
        redirect: {
            name: 'Employees'
        },
        children: Employees
    },
    {
        path: 'EmployeePerformance',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/Employees/EmployeePerformance"),
        redirect: {
            name: 'EmployeePerformance'
        },
        children: EmployeePerformance
    },
    {
        path: 'View-Goals',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/PerformanceList/ViewGoals"),
        redirect: {
            name: 'ViewGoals'
        },
        children: ViewGoals
    },
    {
        path: 'Input-Report',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/PerformanceList/InputReport"),
        redirect: {
            name: 'InputReport'
        },
        children: InputReport
    },
    {
        path: 'Create-Goal',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/PerformanceList/CreateGoal"),
        redirect: {
            name: 'CreateGoal'
        },
        children: CreateGoal
    },
    {
        path: 'Create-Objective',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/PerformanceList/CreateObjective"),
        redirect: {
            name: 'CreateObjective'
        },
        children: CreateObjective
    },
    {
        path: 'View-Objective',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/PerformanceList/ViewObjective"),
        redirect: {
            name: 'ViewObjective'
        },
        children: ViewObjective
    },
    {
        path: 'Create-Category',
        component: () =>
            import ("../../../views/DashboardLinks/Performance/PerformanceList/CreateCategory"),
        redirect: {
            name: 'CreateCategory'
        },
        children: CreateCategory
    },
]


export default Performance