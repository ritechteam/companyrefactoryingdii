const PerformanceOverview = [{
    path: "",
    name: "Overview",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/Overview/Overview"),
    meta: {
        requiresHeader: "Jobs",
    },
},
];

export default PerformanceOverview;