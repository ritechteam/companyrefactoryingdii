const ViewObjective = [{
    path: "",
    name: "ViewObjective",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/ViewObjective"),
    meta: {
        requiresHeader: "Jobs",
    },
}
];

export default ViewObjective;