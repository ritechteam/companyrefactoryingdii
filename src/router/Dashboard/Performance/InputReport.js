const InputReport = [{
    path: "",
    name: "InputReport",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/InputReport"),
    meta: {
        requiresHeader: "Jobs",
    },
}
];

export default InputReport;