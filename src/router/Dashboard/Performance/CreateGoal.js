const CreateGoal = [{
    path: "",
    name: "CreateGoal",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/CreateGoal"),
    meta: {
        requiresHeader: "Jobs",
    },
}
];

export default CreateGoal