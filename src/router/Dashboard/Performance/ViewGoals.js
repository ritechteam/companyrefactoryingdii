const ViewGoals = [{
    path: "",
    name: "ViewGoals",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/PerformanceList/ViewGoals"),
    meta: {
        requiresHeader: "Jobs",
    },
}
];

export default ViewGoals;