const EmployeePerformance = [{
    path: "",
    name: "EmployeePerformance",
    component: () =>
        import ("../../../views/DashboardLinks/Performance/Employees/EmployeePerformance"),
    meta: {
        requiresHeader: "Jobs",
    },
}
];

export default EmployeePerformance;