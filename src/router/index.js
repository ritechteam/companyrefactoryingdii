import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index";
import PublicLayout from "../Layouts/PublicLayout";
import DashboardLayout from "../Layouts/DashboardLayout";
import PublicRoutes from "./Public";
import RecuitmentLinks from "./Dashboard/Recruitment/RecruitmentLinks";
import EmployeeLinks from "./Dashboard/Employee/Employee";
import PerformanceLinks from './Dashboard/Performance/Performance'
import PayrollLinks from "./Dashboard/Payroll/Payroll";
import MainSettings from "./Dashboard/MainSettings";

import FirstOffer from "../Layouts/FirstOffer";
import Home from "../Layouts/Home";
import FAQ from "../Layouts/FAQ";
import OtherRoutes from "./Public/others";
import HomeRoutes from "./Public/home";

Vue.use(VueRouter);


const routes = [
	{
		path: "/",
		component: Home,
	},
	{
		path: "/faq",
		component: FAQ,
	},
	{
		path: "/login",
		component: PublicLayout,
		redirect: {
			name: "Login",
		},
		children: PublicRoutes,
	},
	{
		path: "/early-access",
		component: FirstOffer,
		redirect: {
			name: "FirstOffer",
		},
		children: OtherRoutes,
	},
	{
		path: "/dashboard",
		component: DashboardLayout,
		meta: {
			requiresAuth: true,
		},
		children: [
			{
				path: "",
				name: "DashboardHome",
				component: () => import("../views/Dashboard/Home.vue"),
				meta: {
					requiresHeader: "home",
				},
			},
			{
				path: "recruitment",
				component: () => import("../views/Dashboard/Recruitment.vue"),
				redirect: {
					name: "RecruitmentActive",
				},
				meta: {
					active: true,
				},
				children: RecuitmentLinks,
			},
			{
				path: "employee",
				component: () => import("../views/Dashboard/Employee.vue"),
				redirect: {
					name: "EmployeeOverviewList",
				},
				meta: {
					active: true,
				},
				children: EmployeeLinks,
			},
			{
				path: 'performance',
				component: () =>
					import ("../views/Dashboard/Performance.vue"),
				redirect: {
					name: 'Overview'
				},
				meta: {
					active: true
				},
				children: PerformanceLinks
			},
			{
				path: "payroll",
				component: () => import("../views/Dashboard/Payroll.vue"),
				redirect: {
					name: "PayrollPayments",
				},
				meta: {
					active: true,
				},
				children: PayrollLinks,
			},
			{
				path: "settings",
				component: () =>
					import("../views/DashboardLinks/Settings/Settings.vue"),
				redirect: {
					name: "SettingsCompany",
				},
				children: MainSettings,
			},
		],
	},
];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes,
});

// router.beforeEach((to, from, next) => {
//     if (to.matched.some((route) => route.meta.requiresAuth)) {
//         if (!store.state.data.isLoggedIn) {
//             next({
//                 path: "/login",
//             });
//         } else {
//             next();
//         }
//     }
//     next();
// });

export default router;
