import Vue from "vue";
import moment from 'moment'


const sortDate = function(value) {
    if (!value) return "";
    return moment(value).format("Do MMM YYYY");
};
const truncate = function(text, length, clamp) {
    clamp = clamp || "...";
    var node = document.createElement("div");
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
};



Vue.filter("sortDate", sortDate);
Vue.filter("truncate", truncate);