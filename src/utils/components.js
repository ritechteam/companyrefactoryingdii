import Vue from "vue";
import Button from "@/components/componentItems/Button";
import DashboardSubLinks from "@/components/componentItems/DashboardSubLinks"
import DashboardFixedHeader from "../components/DashboardFixedHeader"
import NotificationDropdown from "../components/dropdownLists/NotificationDropdown"
import UserDropdown from "../components/dropdownLists/UserDropdown"
import Dropdown from "../components/componentItems/Dropdown"
import SubHeader from "../components/componentItems/SubHeader.vue"
import RecruitmentAction from "../components/dropdownLists/RecruitmentAction"
import Table from "../components/componentItems/Table"
import BackButton from "../components/componentItems/BackButton"
import ContainerColumn from "../components/componentItems/ContainerColumn"
import Editor from "../components/componentItems/Editor.vue"
import JobCandidatesHeader from "../components/componentItems/JobCandidatesHeader"
import JobView from "../components/componentItems/JobView"
import ApplicantInfoHeader from "../components/componentItems/ApplicantInfoHeader"
import CategoryBlock from '../components/componentItems/CategoryBlock'
import CategoryMain from '../components/dropdownLists/CategoryMain'
import CategorySubBlock from '../components/dropdownLists/CategorySubBlock'
import RemoveAssessmentDropdown from '../components/dropdownLists/RemoveAssessmentDropdown'
import Modal from '../components/componentItems/Modal.vue'
import ActiveJobDropdown from '../components/dropdownLists/ActiveJob.vue'
import DraftJobDropdown from '../components/dropdownLists/DraftJob.vue'
import ArchivedJobDropdown from '../components/dropdownLists/ArchivedJob.vue'
import PerformanceGoalsDropdown from '../components/dropdownLists/PerformanceGoals.vue'
import PerformanceObjectivesDropdown from '../components/dropdownLists/PerformanceObjectives.vue'
import MyPerformanceDropdown from '../components/dropdownLists/MyPerformanceDropdown.vue'
import TaskDropdown from '../components/dropdownLists/TaskDropdown.vue'
import FeedbackDropdown from '../components/dropdownLists/FeedbackDropdown.vue'
import ApplicationsDropdown from '../components/dropdownLists/ApplicationsDropdown.vue'
import StatutoryBodyDropdown from '../components/dropdownLists/StatutoryBodyDropdown.vue'
import PaymentsDropdown from '../components/dropdownLists/PaymentsDropdown.vue'
import PayrollEmployeesDropdown from '../components/dropdownLists/PayrollEmployeesDropdown.vue'


Vue.component("easiButton", Button);
Vue.component("easiSubLinks", DashboardSubLinks);
Vue.component("easiDashboardFixedHeader", DashboardFixedHeader);
Vue.component("easiDropdown", Dropdown);
Vue.component("easiUserDropdown", UserDropdown);
Vue.component("easiNotificationDropdown", NotificationDropdown);
Vue.component("easiSubHeader", SubHeader);
Vue.component("easiRecruitmentAction", RecruitmentAction);
Vue.component("easiTable", Table);
Vue.component("easiBackButton", BackButton);
Vue.component("easiContainerColumn", ContainerColumn);
Vue.component("easiEditor", Editor);
Vue.component("easiJobCandidatesHeader", JobCandidatesHeader);
Vue.component("easiJobView", JobView);
Vue.component('easiApplicantInfoHeader', ApplicantInfoHeader)
Vue.component("easiCategoryBlock", CategoryBlock);
Vue.component("easiCategoryMain", CategoryMain);
Vue.component("easiCategorySubBlock", CategorySubBlock);
Vue.component("easiRemoveAssessmentDropdown", RemoveAssessmentDropdown);
Vue.component("easiModal", Modal);
Vue.component("easiActiveJobDropdown", ActiveJobDropdown);
Vue.component("easiDraftJobDropdown", DraftJobDropdown);
Vue.component("easiArchivedJobDropdown", ArchivedJobDropdown);
Vue.component("easiPerformanceGoalsDropdown", PerformanceGoalsDropdown);
Vue.component("easiPerformanceObjectivesDropdown", PerformanceObjectivesDropdown);
Vue.component("easiMyPerformanceDropdown", MyPerformanceDropdown);
Vue.component("easiTaskDropdown", TaskDropdown);
Vue.component("easiFeedbackDropdown", FeedbackDropdown);
Vue.component("easiApplicationsDropdown", ApplicationsDropdown);
Vue.component("easiStatutoryBodyDropdown", StatutoryBodyDropdown);
Vue.component("easiPaymentsDropdown", PaymentsDropdown);
Vue.component("easiPayrollEmployeesDropdown", PayrollEmployeesDropdown);

