import Vue from "vue";
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { onError } from "apollo-link-error";
import store from "../store/index";
import router from "../router";

export const AUTH_CONTEXT = () => {
    const token = localStorage.getItem("token");
    return token ? `Bearer ${token}` : "";
};

export const logout = () => {
    localStorage.removeItem("token");
    store.commit("set", {
        type: "isLoggedIn",
        data: false,
    });
    store.commit("set", {
        type: "applicant",
        data: "",
    });
    router.push("/login");
};

const errorLink = onError((errors) => {
    const { graphQLErrors, networkError } = errors;
    if (graphQLErrors && graphQLErrors.length > 0) {
        let e = graphQLErrors[0];
        Vue.$toast.error(e.message, {
            position: "bottom",
            duration: 3000,
        });
    }

    if (networkError == "ServerError: Response not successful: Received status code 401") {
        Vue.$toast.error(`${networkError}, Logging User Out`, {
            position: "bottom",
            duration: 3000,
        });
        logout();
    }
});

// const httpLink = createHttpLink({
//     uri: `https://api.r-impact.com/query`,
//     // uri: `http://134.122.31.217/query`,
// });

// const link = errorLink.concat(httpLink);

// export const API = new ApolloClient({
//     link,
//     cache: new InMemoryCache(),
//     defaultOptions: {
//         watchQuery: {
//             fetchPolicy: "no-cache",
//             errorPolicy: "ignore",
//         },
//         query: {
//             fetchPolicy: "no-cache",
//             errorPolicy: "all",
//         },
//     },
// });


function getService(link) {
    const API = new ApolloClient({
        link,
        cache: new InMemoryCache(),
        defaultOptions: {
            watchQuery: {
                fetchPolicy: "no-cache",
                errorPolicy: "ignore",
            },
            query: {
                fetchPolicy: "no-cache",
                errorPolicy: "all",
            },
        },
    });

    return API;
}


var httplink = createHttpLink({
    uri: `https://eazihires-auth.herokuapp.com/graphql`,
});
const authHttpLink = errorLink.concat(httplink);

var httplink_emp = createHttpLink({
    uri: `https://eazihires-emp.herokuapp.com/graphql`,
});
const empHttpLink = errorLink.concat(httplink_emp);

var httplink_pay = createHttpLink({
    uri: `https://eazihires-payroll.herokuapp.com/graphql`,
});
const payHttpLink = errorLink.concat(httplink_pay);

var httplink_perform = createHttpLink({
    uri: `https://eazihires-performance.herokuapp.com/graphql`,
});
const performHttpLink = errorLink.concat(httplink_perform);

var httplink_recruit = createHttpLink({
    uri: `https://eazihires-recruit.herokuapp.com/graphql`,
});
const recruitHttpLink = errorLink.concat(httplink_recruit);



const AUTH_API = getService(authHttpLink);
const EMP_API = getService(empHttpLink);
const PAYROLL_API = getService(payHttpLink);
const PERFORMANCE_API = getService(performHttpLink);
const RECRUITMENT_API = getService(recruitHttpLink);

export const API = {AUTH_API, EMP_API, PAYROLL_API, PERFORMANCE_API, RECRUITMENT_API}