import queries from "./queries";
// import { API, AUTH_CONTEXT } from "./api";
import { API, AUTH_CONTEXT } from "./api";

export default (endpoint, payload, service) => {
    let token = localStorage.getItem("token");
    let context = {};
    if (token !== "" && token !== null && token !== undefined) {
        context.headers = {
            authorization: AUTH_CONTEXT(),
        };
    }

    const PAYLOAD = {
        query: queries(endpoint),
        variables: payload,
        context,
    };

    if (service == "AUTH"){
        return API.AUTH_API.query(PAYLOAD).then((result) => result.data[endpoint]);
    }else if (service == "EMP"){
        return API.EMP_API.query(PAYLOAD).then((result) => result.data[endpoint]);
    }else if (service == "PAY"){
        return API.PAYROLL_API.query(PAYLOAD).then((result) => result.data[endpoint]);
    } else if(service == "PERFORM"){
        return API.PERFORMANCE_API.query(PAYLOAD).then((result)=> result.data[endpoint]);
    } else if(service == "RECRUIT"){
        return API.RECRUITMENT_API.query(PAYLOAD).then((result) => result.data[endpoint]);
    }
};