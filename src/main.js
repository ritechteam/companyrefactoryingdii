import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueApollo from 'vue-apollo';
import VueToast from 'vue-toast-notification';
import { API } from './api/api';
import './assets/styles/index.css';
import 'vue-toast-notification/dist/theme-default.css';
import './utils/components.js';
import './utils/filters.js';
import 'vue-step-progress/dist/main.css';

import "@/assets/global.css" //Global CSS

Vue.use(VueToast);
Vue.use(VueApollo);
Vue.config.productionTip = false;

const apolloProvider = new VueApollo({
	defaultClient: API,
});

new Vue({
	apolloProvider,
	router,
	store,
	render: (h) => h(App),
}).$mount('#app');
