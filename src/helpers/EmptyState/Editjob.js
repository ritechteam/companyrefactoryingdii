const editJobs = {
	id: '',
	title: '',
	intro: '',
	keyActivities: '',
	skillsAndRequirements: '',
	perksAndBenefits: '',
	minQualification: '',
	salaryCurrency: '',
	country: '',
	state: '',
	status: '',
	others: '',
	employment: '',
	level: '',
	minYearsOfExperience: '',
	minSalary: '',
	maxSalary: '',
	jobCategory: '',
	assessmentCategoryId: '',
};

export default editJobs;
