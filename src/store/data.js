import { MUTATE, QUERY } from '../api';
import createJobs from '../helpers/EmptyState/Createjob';
import editJobs from '../helpers/EmptyState/Editjob';
import CreateEmployee from '../helpers/EmptyState/CreateEmployee';

export default {
	state: {
		applicant: '',
		isLoggedIn: false,
		loading: false,
		currentRoute: '',
		createJobs,
		editJobs,
		CreateEmployee,
		activeJobs: {
			count: 0,
			data: [],
		},
		draftJobs: {
			count: 0,
			data: [],
		},
		archivedJobs: {
			count: 0,
			data: [],
		},
		listAssessments: {
			count: 0,
			data: [],
		},
		listAssessmentCategory: {
			count: 0,
			data: [],
		},
		questions: {
			assessmentId: '',
			questions: '',
		},
		assessmentToCategoryPayload: {
			assessmentId: '',
			assessmentCategoryId: '',
		},

		// Note: Recruitment - Candidates
		candidates: {
			count: 0,
			data: [],
		},

		listCategories: {
			count: 0,
			data: [],
		},
		listObjectives: {
			count: 0,
			data: [],
		},

		listGoals: {
			count: 0,
			data: [],
		},
		performanceGoalView: '',

		listRatings: {
			count: 0,
			data: [],
		},
		averageRatings: {
			averageRating: 0,
			employeeID: ''
		},
		averageGoalRatings: {
			averageRating: 0,
			employeeID: '',
			goalID: ''
		},
		employeePerformance: {
			count: 0,
			data: []
		},
		employeeGoal: {
			count: 0,
			data: []
		},
		employeeGoalView: '',

		listDepartments: {
			count: 0,
			data: [],
		},
		listEmployees: {
			count: 0,
			data: [],
		},
		
		listMemoTemplates: {
			count: 0,
			data: [],
		},
		listMemos: {
			count: 0,
			data: [],
		},

		listTasks: {
			count: 0,
			data: [],
		},
		listReceivedTasks: {
			count: 0,
			data: [],
		},
		listCompletedTasks: {
			count: 0,
			data: [],
		},

		listIds: '',
		viewEmployee: '',
		createDocumentFolder: '',
		listApplicants: {
			count: 0,
			data: [],
		},
		listStaffLevel: {
			count: 0,
			data: [],
		},
		viewJob: '',
		viewInfo: '',
		currentApplicant: '',
		benefits: '',
		createStaffLevel: {
			title: '',
			benefitAmounts: '',
			departmentStaffsInput: '',
		},

		// Employee Management - Feedback
		receivedFeedback: {
			count: 0,
			data: []
		},
		sentFeedback: {
			count: 0,
			data: []
		},
		allFeedback: {
			count: 0,
			data: []
		},
		feedbackView: '',

		// Employee Management - Applications
		receivedApplications: {
			count: 0,
			data: []
		},
		sentApplications: {
			count: 0,
			data: []
		},
		allApplications: {
			count: 0,
			data: []
		},
		applicationsView: '',

		// Payroll - Settings
		statutoryBody: {
			count: 0,
			data: [],
		},
		employeeStatutoryBody: {
			count: 0,
			data: [],
		},
		employeeStatutoryBodyView: '',

		statutoryBodyByProvision: {
			count: 0,
			data: [],
		},
		statutoryBodyByProvisionView: '',
		
		provision: {
			count: 0,
			data: [],
		},
		provisionView: '',

		allowance: {
			count: 0,
			data: [],
		},
		allowanceView: '',

		benefit: {
			count: 0,
			data: [],
		},
		benefitView: '',

		otherPayment: {
			count: 0,
			data: [],
		},
		otherPaymentView: '',

		team: {
			count: 0,
			data: [],
		},
		teamView: '',

		branch: {
			count: 0,
			data: [],
		},
		branchView: '',

		staffAccessLevel: {
			count: 0,
			data: [],
		},
		staffAccessLevelView: '',

		paye: {
			count: 0,
			data: [],
		},
		payeByMonth: {
			count: 0,
			data: [],
		},
		payeView: '',

		nhf: {
			count: 0,
			data: [],
		},
		nhfByMonth: {
			count: 0,
			data: [],
		},
		nhfView: '',

		lifeInsurance: {
			count: 0,
			data: [],
		},
		lifeInsuranceByMonth: {
			count: 0,
			data: [],
		},
		lifeInsuranceView: '',

		pension: {
			count: 0,
			data: [],
		},
		pensionByMonth: {
			count: 0,
			data: [],
		},
		pensionView: '',

		// Settings - Company Bank Details
		companyBankDetails: {
			count: 0,
			data: [],
		},
		companyBankDetailsView: '',

		// Compliance:
		companyComplianceView: '',

		employeeReferee: {
			count: 0,
			data: [],
		},
		employeeRefereeView: '',

		// Privileges:
		role: {
			count: 0,
			data: [],
		},
		roleView: '',
		employeerole: {
			count: 0,
			data: [],
		},

		// Payroll monthly
		payrollByMonth: {
			count: 0,
			data: [],
		},

		// Get All:
		allCompanyAllowancesView: '',
		allCompanyBenefitsView: '',
		allCompanyStatutoryBodies: '',

		// View company
		companyView: '',


	},

	getters: {
		applicant: (state) => state.applicant,
		getCurrentRoute: (state) => state.currentRoute,
		jobCreation: (state) => state.createJobs,
		editJob: (state) => state.editJobs,
		getActiveJobs: (state) => state.activeJobs,
		getDraftJobs: (state) => state.draftJobs,
		getArchivedJobs: (state) => state.archivedJobs,
		checkLoading: (state) => state.loading,
		listAllAssessments: (state) => state.listAssessments,
		listAllAssessmentCategories: (state) => state.listAssessmentCategory,
		getQuestions: (state) => state.questions,
		getAssessmentCategoryPayload: (state) => state.assessmentToCategoryPayload,
		getAllDepartments: (state) => state.listDepartments,
		createNewEmployee: (state) => state.CreateEmployee,
		getAllEmployees: (state) => state.listEmployees,
		getEmployee: (state) => state.viewEmployee,
		allIds: (state) => state.listIds,
		getDocumentFolder: (state) => state.createDocumentFolder,
		getAllApplicants: (state) => state.listApplicants,

		listCandidates: (state) => state.candidates,

		getAllMemoTemplates: (state) => state.listMemoTemplates,
		getAllMemos: (state) => state.listMemos,
		

		getCurrentJob: (state) => state.viewJob,
		currentInfo: (state) => state.viewInfo,
		getCurrentApplicant: (state) => state.currentApplicant,
		getBenefits: (state) => state.benefits,
		getStaffLevelData: (state) => state.createStaffLevel,
		listAllStaffLevels: (state) => state.listStaffLevel,
		allPerformanceCategories: (state) => state.listCategories,
		allPerformanceObjectives: (state) => state.listObjectives,
		allPerformanceGoals: (state) => state.listGoals,
		viewPerformanceGoals: (state) => state.performanceGoalView,

		getAllTasks: (state) => state.listTasks,
		getReceivedTasks: (state) => state.listReceivedTasks,
		getCompletedTasks: (state) => state.listCompletedTasks,


		allPerformanceRatings: (state) => state.listRatings,

		getRatingBasedOnEmployee: (state) => state.averageRatings,
		getRatingBasedOnGoalAndEmployee: (state) => state.averageGoalRatings,
		listEmployeePerformanceRatings: (state) => state.employeePerformance,
		employeeGoalList: (state) => state.employeeGoal,
		viewEmployeeGoal: (state) => state.employeeGoalView,
		
		listSentFeedback: (state) => state.sentFeedback,
		listReceivedFeedback: (state) => state.receivedFeedback,
		listAllFeedback: (state) => state.allFeedback,
		viewFeedback: (state) => state.feedbackView,

		listSentApplications: (state) => state.sentApplications,
		listReceivedApplications: (state) => state.receivedApplications,
		listAllApplications: (state) => state.allApplications,
		viewApplications: (state) => state.applicationsView,

		listStatutoryBody: (state) => state.statutoryBody,
		listEmployeeStatutoryBody: (state) => state.employeeStatutoryBody,
		viewEmployeeStatutoryBody: (state) => state.employeeStatutoryBodyView,

		listStatutoryBodyByProvision: (state) => state.statutoryBodyByProvision,
		viewStatutoryBodyByProvision: (state) => state.statutoryBodyByProvisionView,
		
		listProvision: (state) => state.provision,
		viewProvision: (state) => state.provisionView,

		listAllowance: (state) => state.allowance,
		viewAllowance: (state) => state.allowanceView,

		listBenefit: (state) => state.benefit,
		viewBenefit: (state) => state.benefitView,

		listOtherPayment: (state) => state.otherPayment,
		viewOtherPayment: (state) => state.otherPaymentView,

		listTeam: (state) => state.team,
		viewTeam: (state) => state.teamView,

		listBranch: (state) => state.branch,
		viewBranch: (state) => state.branchView,

		listStaffAccessLevel: (state) => state.staffAccessLevel,
		viewStaffAccessLevel: (state) => state.staffAccessLevelView,
		
		listPaye: (state) => state.paye,
		listPayeByMonth: (state) => state.payeByMonth,
		viewPaye: (state) => state.payeView,

		listNHF: (state) => state.nhf,
		listNHFByMonth: (state) => state.nhfByMonth,
		viewNHF: (state) => state.nhfView,

		listLifeInsurance: (state) => state.lifeInsurance,
		listLifeInsuranceByMonth: (state) => state.lifeInsuranceByMonth,
		viewLifeInsurance: (state) => state.lifeInsuranceView,

		listPension: (state) => state.pension,
		listPensionByMonth: (state) => state.pensionByMonth,
		viewPension: (state) => state.pensionView,
		
		listCompanyBankDetails: (state) => state.companyBankDetails,
		viewCompanyBankDetails: (state) => state.companyBankDetailsView,

		viewCompanyCompliance: (state) => state.companyComplianceView,

		// Employee Referee
		listEmployeeReferee: (state) => state.employeeReferee,
		viewEmployeeReferee: (state) => state.employeeRefereeView,

		// Privileges:
		listRole: (state) => state.role,
		viewRole: (state) => state.roleView,
		listEmployeeRole: (state) => state.employeerole,

		// Payroll Monthyl
		listPayrollByMonth: (state) => state.payrollByMonth,
		

		// Get all:
		getAllCompanyAllowances: (state) => state.allCompanyAllowancesView,
		getAllCompanyBenefits: (state) => state.allCompanyBenefitsView,
		getAllCompanyStatutoryBodies: (state) => state.allCompanyStatutoryBodies,

		// View company
		viewCompany: (state) => state.companyView,
	},

	mutations: {
		set: (state, { type, data }) => {
			const keys = Object.keys(state);
			for (let i = 0; i < keys.length; i++) {
				state[keys[i]] = keys[i] === type ? data : state[keys[i]];
			}
			return state;
		},

		resetDefault: (state, { value, type }) => {
			const keys = Object.keys(state);
			let data = keys.find((x) => {
				return x == value;
			});
			let object = state[data];
			let setAll = (obj, val) =>
				Object.keys(obj).forEach((k) => (obj[k] = val));
			setAll(object, type);
		},

		populate(state) {
			let title = [
				'FrontEnd Developer',
				'BackEnd Developer',
				'Dev-ops',
				'Designer',
				'Project Lead',
				'Accounting',
			];
			let jobCategory = [
				'INTERNSHIP',
				'CONTRACT',
				'FREELANCE',
				'FULLTIME',
				'PARTTIME',
			];
			let currency = 'NGN';
			let salary = [120000, 350000, 450000, 100000, 300000];
			let maxSalary = [350000, 400000, 700000, 1000000];
			let type = [
				'FULLTIME_ONSITE',
				'CONTRACT_ONSITE',
				'FULLTIME_REMOTE',
				'CONTRACT_REMOTE',
			];
			let country = 'Nigeria';
			let State = 'Lagos';
			let jobLevel = ['ENTRY', 'INTERMEDIATE', 'EXPERIENCED'];
			let experience = [2, 3, 4, 5, 6, 7];
			let qualification = ['SSCE', 'BACHELOR', 'MASTERS', 'PhD', 'None'];
			let others =
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ';
			let k = function rando(min, max) {
				return Math.floor(Math.random() * (max - min + 1) + min);
			};

			state.createJobs.title = title[k(0, title.length - 1)];
			state.createJobs.intro = others;
			state.createJobs.keyActivities = others;
			state.createJobs.skillsAndRequirements = others;
			state.createJobs.perksAndBenefits = others;
			state.createJobs.minQualification =
				qualification[k(0, qualification.length - 1)];
			state.createJobs.salaryCurrency = currency;
			state.createJobs.country = country;
			state.createJobs.state = State[k(0, state.createJobs.state.length - 1)];
			state.createJobs.employment = type[k(0, type.length - 1)];
			state.createJobs.level = jobLevel[k(0, jobLevel.length - 1)];
			state.createJobs.minYearsOfExperience =
				experience[k(0, experience.length - 1)];
			state.createJobs.minSalary = salary[k(0, salary.length - 1)];
			state.createJobs.maxSalary = maxSalary[k(0, salary.length - 1)];

			state.createJobs.others = others;
			state.createJobs.status = 'OPEN';
			state.createJobs.jobCategory = jobCategory[k(0, salary.length - 1)];
		},
	},

	actions: {
		// Service helps to determine what graphql server to point to.
		mutate: ({ commit, dispatch }, { endpoint, data, service }) =>
			MUTATE(endpoint, data, service),

		setQuery: ({ commit, dispatch }, { endpoint, data, service }) =>
			QUERY(endpoint, data, service),

		query: ({ commit, dispatch }, { endpoint, payload, storeKey, service }) => {
			commit('set', {
				type: 'loading',
				data: true,
			});
			return QUERY(endpoint, payload)
				.then((res) => {
					commit('set', {
						type: 'loading',
						data: false,
					});
					commit('set', {
						type: storeKey,
						data: res,
					});
				})
				.catch((err) => {
					console.log(err, ' The Error');
				});
		},
	},
};
