import Vue from 'vue';
export default {
	state: {
		countries: '',
		openJobModal: false,
		editAssessmentModal: false,
		editAssessmentCategoryModal: false,
		addAssessmentToCategoryModal: false,
		departmentModal: false,
		editPayload: '',
		finishQuestion: false,
		questionState: {
			multipleQuestion: true,
			writingQuestion: true,
			trueorfalseQuestion: true,
		},
		editAssesmentPayload: {
			id: '',
			name: '',
			description: '',
		},
		editAssessmentData: {
			id: '',
			level: '',
			title: '',
			duration: '',
		},
		editDepartmentModal: {
			id: '',
			name: '',
			description: '',
		},
	},

	getters: {
		getCountries: (state) => state.countries,
		jobModal: (state) => state.openJobModal,
		editAssessmentModal: (state) => state.editAssessmentModal,
		editAssessmentCategoryModal: (state) => state.editAssessmentCategoryModal,
		editAssesmentPayload: (state) => state.editAssesmentPayload,
		editAssesmentData: (state) => state.editAssessmentData,
		getQuestionState: (state) => state.questionState,
		getFinishQuestion: (state) => state.finishQuestion,
		assessmentToCategory: (state) => state.addAssessmentToCategoryModal,
		departmentModal: (state) => state.departmentModal,
		getEditDepartmentModal: (state) => state.editDepartmentModal,
	},

	mutations: {
		setCountries(state, payload) {
			state.countries = payload;
		},
		setSuccess({}, { message, value, type }) {
			Vue.$toast.success(message, {
				position: 'bottom',
				duration: 3000,
			});
			this.commit('resetDefault', { value, type });
		},

		clearEditAssessmentPayload(state, payload) {
			(state.editAssesmentPayload.id = ''),
				(state.editAssesmentPayload.name = ''),
				(state.editAssesmentPayload.description = '');
		},
		clearEditAssessmentData(state, payload) {
			(state.editAssessmentData.id = ''),
				(state.editAssessmentData.level = ''),
				(state.editAssessmentData.title = ''),
				(state.editAssessmentData.duration = '');
		},
		clearEditDepartment(state) {
			(state.editDepartmentModal.name = ''),
				(state.editDepartmentModal.id = '');
			state.editDepartmentModal.description = '';
		},

		change: (state, { type, data }) => {
			const keys = Object.keys(state);
			for (let i = 0; i < keys.length; i++) {
				state[keys[i]] = keys[i] === type ? data : state[keys[i]];
			}
			return state;
		},
	},

	actions: {
		async getCountries({ commit }) {
			let res = await fetch(
				'https://api.jsonbin.io/b/5f0c5303a62f9b4b276421b0',
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						'secret-key':
							'$2b$10$pJrLQoiTwJcs855o2sd2v.EDREo1BJffY5EM5Gp.BeNZxctGlCHna',
					},
				}
			);
			let newRes = await res.json();
			commit('setCountries', newRes);
			return newRes;
		},
	},
};
