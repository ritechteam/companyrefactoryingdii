import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import auth from './auth';
import data from './data';
import helper from './helper';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		auth,
		data,
		helper,
	},
	plugins: [
		createPersistedState({
			paths: [
				'data.applicant',
				'data.isLoggedIn',
				'data.currentRoute',
				'helper.countries',
				'data.questions',
				'data.listAssessmentCategory',
				'data.listAssessments',
				'data.CreateEmployee',
				'data.viewEmployee',
				'data.viewInfo',
				'data.benefits',
				'data.createStaffLevel',
				'data.listDepartments',
				'data.listStaffLevel',
			],
		}),
	],
});
