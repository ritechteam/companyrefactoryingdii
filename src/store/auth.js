import { MUTATE } from "../api/index";

const actions = {
    //loginUser: (state = {}, loginPayload) => MUTATE("Login", loginPayload), //Previous endpoint method.
    loginUser: (state = {}, loginPayload) => MUTATE("EmployeeLogin", loginPayload, "AUTH"),
    registerCompany: (state = {}, registerPayload) => MUTATE("RegisterCompany", registerPayload, "AUTH"),
    forgotPassword: (state = {}, forgotPasswordPayload) => MUTATE("RequestPasswordReset", forgotPasswordPayload),
    changePassword: (state = {}, resetPasswordPayload) => MUTATE("ResetPassword", resetPasswordPayload),
};

export default {
    actions,
};